# Online Registration Application #

Develop an Online Registration Application which enables the submission of a form. 
The form must capture the following fields:
* A Person’s Full Name
* A Person’s ID Number

The form must be submitted to a backend Restful API based service to be:
* Validated
* Persisted in memory

Invalid data should not be persisted.

- Develop an online report to retrieve all the submitted forms and display them.
- Write tests if applicable.
- Online form and report can be developed using any technology stack.
- Backend service must be developed in Java (Version 8 or later) 

The source code and instructions to build and run the system should be committed into a single Git-based repository, 
accessible for us to clone and review. 
You may use any online resources as a reference to complete this task but do not copy code.

Please email the URL to your Git repository once you complete the application in the agreed timeframe. 


### Remote Database Details ###

* URL: 164.160.91.12
* Username: prologiq_assessment
* Password: v8vNgQaL6L5VpHUX
* Database: prologiq_iqapp

